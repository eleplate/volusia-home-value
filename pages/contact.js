import Head from 'next/head'
import Button from '@material-ui/core/Button'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import React, { useState, useEffect, useRef } from "react";
import SearchLocationInput from '../components/searchLocationInput.js';
import test from '../components/test.js';
import Modal from '../components/modal.js';
import ContactForm from '../components/contactForm';
import Footer from '../components/footer.js';


export default function Home() {


  return (
    <div className={styles.container}>
      <Head>
        <title>Volusia Home Value</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
      <h1 className={styles.title}>
          Contact <br />
        </h1>
        <h1 className={styles.colorTitle}>
          Volusia Home Value
        </h1>
        <p />
        <ContactForm />
        {/* <div className={styles.searchLocationInput}>
            <input placeholder='Name' />
        </div>
        <br />
        <div className={styles.searchLocationInput}>
            <input placeholder='Email' />
        </div>
        <br />
        <div className={styles.messageInput}>
            <input placeholder='How can we help?' />
        </div>
        <p />
        <Link href="/" passHref>
          <Button variant="contained" color="secondary">Submit</Button>
        </Link> */}

        <p className={styles.description}>
          <span className={styles.colorDescription}>
            We will get back you within 24 hours. 
          </span>
          {/* <code className={styles.code}>your address</code> */}
        </p>
       
        <div className={styles.grid}>
          <a href="/" className={styles.card}>
            <h3>Home &rarr;</h3>
            <p>Learn the value of your home for free by entering your address.</p>
          </a>

          <a href="/about" className={styles.card}>
            <h3>About &rarr;</h3>
            <p>Learn how Volusia Home Value is the best in the business.</p>
          </a>
        </div>
        

    </main>

    <Footer />
    </div>
  )
}
