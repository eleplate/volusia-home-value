import '../styles/globals.css'
import React from 'react';
import { render } from 'react-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: '#0070f3'
    }
  }
});

function MyApp({ Component, pageProps }) {
  return(
    
    <MuiThemeProvider theme={theme}>
        <Component {...pageProps} />
    </MuiThemeProvider>
    
  )
  
  
}
export default MyApp;

